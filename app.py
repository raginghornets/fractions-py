#!/usr/bin/env python3

if __name__ == '__main__':
  from fractions import fraction

  myFraction = fraction.Fraction(int(input('Numerator: ')), int(input('Denominator: ')))

  print('str: ' + str(myFraction))
  print('repr: ' + str(repr(myFraction)))
  print('simplified: ' + str(myFraction.simplify()))
  print('decimal: ' + str(myFraction.decimal()))
