#!/usr/bin/env python3

import math

class Fraction:
  def __init__(self, numerator, denominator):
    self.numerator = numerator
    self.denominator = denominator
  
  def __repr__(self):
    return 'Fraction({!r},{!r})'.format(self.numerator, self.denominator)
 
  def __str__(self):
    return str(self.numerator) + '/' + str(self.denominator)   

  def simplify(self):
    gcd = math.gcd(self.numerator, self.denominator)
    self.numerator /= gcd
    self.denominator /= gcd
    return self
  
  def decimal(self):
    return float(self.numerator) / self.denominator
